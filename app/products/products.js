const express = require("express");
const router = express.Router();

const fs = require("fs");
let pathes = [];
let products = [];

fs.readdir("./products_db", (err, files) => {
  if (!err) {
    files.forEach(file => {
      pathes.push("./products_db/" + file);
    });
    let lastFive = pathes.slice(Math.max(pathes.length - 5, 1));

    lastFive.forEach(path => {
        fs.readFile(path, (err, result) => {
            if (!err) {
            products.push(JSON.parse(result));
            }
        });
    });
  } else {
    throw err;
  }
});
    
router.get("/", (req, res) => {
  console.log("last five", products);
  res.send(products);
});
router.post("/", (req, res) => {
  let date = new Date();
  let product = req.body;
  product.datenow = date.toISOString();
  fs.writeFile(`./products_db/${date.toISOString()}.json`, JSON.stringify(product),
    err => {
      if (err) {
        throw err;
      } else {
        console.log("file was created");
      }
    }
  );
});

module.exports = router;
