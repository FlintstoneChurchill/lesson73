const fs = require('fs');

const db = {
    data: [],
    init: (callback) => {
        fs.readFile('./products.json', (err, res) => {
            if(err) {
                throw err;
            } 
            this.data = JSON.parse(res.toString());
            callback();
        });
    }
};

module.exports = db;