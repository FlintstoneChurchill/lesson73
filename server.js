const express = require('express');
const app = express();
const products = require('./app/products/products');

// const db = require('./db');

const port = 8000;

app.use(express.json());


app.use('/products', products);

app.listen(port, () => {
    console.log('Server started on ' + port + ' port');
});

// db.init(() => {
// });

